# Todool

An example web app that shows how to use
Batman.js as a client side framework for
building mobile web applications.

Application Server written in noir.

# Files of Interest

## app.coffee

This is the client code that interfaces with the
API.  It is the web front end that an end user
of the application would see.

## todo.clj

This is the API of the application.  It shows all
the endpoints and what operations are permitted.

## index.clj

This is the view that has been constructed for Batman.js
to interact with.  It uses [Hiccup](https://github.com/weavejester/hiccup)
for the templating language.

# Dependencies & Installation

In order to run the application you will need to install
[leiningen](https://github.com/technomancy/leiningen) in
order to properly resolve all of the clojure dependencies.

Once you have installed leiningen you simply need to enter
the following commands to get the server running.

> lein deps

> lein run


Now just open your web browser to localhost:8080 to see
the Batman powered application in all it's glory.

## License

Copyright (C) 2011 Christopher Saunders

Distributed under the Eclipse Public License, the same as Clojure.

