class Todoodl extends Batman.App
  @global yes

  @root 'todos#index'

# Model
class Todoodl.Todo extends Batman.Model
  @global yes

  @persist Batman.RestStorage
  @url = "/todos"

  @encode 'task', 'done'

  @classAccessor 'remainingTasks', ->
    @get('all').filter (p) -> if p.get('done') == false then p else null

  confirmDestroy: ->
    confirmation = confirm("Are you sure you want to delete this task?")
    @destroy() if confirmation

  task: ''
  done: false

# Controller
class Todoodl.TodosController extends Batman.Controller
  emptyTodo: null

  index: ->
    @set 'emptyTodo', new Todo

    Todo.load (error, todos) ->
      throw error if error

    @render false

  create: =>
    @emptyTodo.save (error, record) =>
      throw error if error

    @set 'emptyTodo', new Todo

Todoodl.run()
