(function() {
  var Todoodl;
  var __hasProp = Object.prototype.hasOwnProperty, __extends = function(child, parent) {
    for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; }
    function ctor() { this.constructor = child; }
    ctor.prototype = parent.prototype;
    child.prototype = new ctor;
    child.__super__ = parent.prototype;
    return child;
  }, __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };
  Todoodl = (function() {
    __extends(Todoodl, Batman.App);
    function Todoodl() {
      Todoodl.__super__.constructor.apply(this, arguments);
    }
    Todoodl.global(true);
    Todoodl.root('todos#index');
    return Todoodl;
  })();
  Todoodl.Todo = (function() {
    __extends(Todo, Batman.Model);
    function Todo() {
      Todo.__super__.constructor.apply(this, arguments);
    }
    Todo.global(true);
    Todo.persist(Batman.RestStorage);
    Todo.url = "/todos";
    Todo.encode('task', 'done');
    Todo.classAccessor('remainingTasks', function() {
      return this.get('all').filter(function(p) {
        if (p.get('done') === false) {
          return p;
        } else {
          return null;
        }
      });
    });
    Todo.prototype.confirmDestroy = function() {
      var confirmation;
      confirmation = confirm("Are you sure you want to delete this task?");
      if (confirmation) {
        return this.destroy();
      }
    };
    Todo.prototype.task = '';
    Todo.prototype.done = false;
    return Todo;
  })();
  Todoodl.TodosController = (function() {
    __extends(TodosController, Batman.Controller);
    function TodosController() {
      this.create = __bind(this.create, this);
      TodosController.__super__.constructor.apply(this, arguments);
    }
    TodosController.prototype.emptyTodo = null;
    TodosController.prototype.index = function() {
      this.set('emptyTodo', new Todo);
      Todo.load(function(error, todos) {
        if (error) {
          throw error;
        }
      });
      return this.render(false);
    };
    TodosController.prototype.create = function() {
      this.emptyTodo.save(__bind(function(error, record) {
        if (error) {
          throw error;
        }
      }, this));
      return this.set('emptyTodo', new Todo);
    };
    return TodosController;
  })();
  Todoodl.run();
}).call(this);
