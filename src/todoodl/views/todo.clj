(ns todoodl.views.todo
  (:require [todoodl.views.common :as common]
            [noir.response :as response]
            [todoodl.models.todo :as todo-model])
  (:use noir.core
        hiccup.core
        hiccup.page-helpers)
  )

(defn json-data [entry]
  (conj entry {:done (. Boolean (parseBoolean (:done entry)))})
  )

(defpage "/todos" {}
  (response/json  (vec (map json-data (todo-model/all))))
  )

(defpage [:post "/todos"] {:keys [todo]}
  (response/json  (json-data (todo-model/create (:task todo))))
  )

(defpage "/todos/:id" {:keys [id]}
  (response/json (json-data (todo-model/find-by-id id))))

(defpage [:put "/todos/:id"] {:keys [id todo]}
  (let
    [task (:task todo)
     done (:done todo)]
   (if-let [model (todo-model/find-by-id id)]
     (let [
           task (if (not= 0 (count task)) task (:task model))
           done (if (not= 0 (count done)) done (:done model))
           ]
       (do
         (todo-model/update (conj model {:task task :done done}))
         (response/json (json-data (todo-model/find-by-id id)))
         )
       )
     (response/empty)))
  )

(defpage [:delete "/todos/:id"] {:keys [id]}
  (if-let [task (todo-model/find-by-id id)]
    (do
      (todo-model/delete task)
      (response/empty))
    (response/status 404 "Not Found")
    )
  )
