(ns todoodl.views.common
  (:use noir.core
        hiccup.core
        hiccup.page-helpers))

(defpartial layout [& content]
            (html5
              [:head
               [:title "todoodl"]
               (include-css "/css/reset.css" "/css/base.css" "/css/layout.css" "/css/skeleton.css" "/css/application.css")
               (include-js "/js/es5-shim.js" "/js/batman.js" "/js/batman.solo.js" "/js/coffee-script.js" "/js/app.js")
               [:meta {:name "apple-mobile-web-app-capable" :content "yes"}]
               [:meta {:name "apple-mobile-web-app-status-bar-style" :content "yes"}]
               [:meta {:name "viewport" :content "initial-scale=1.0,user-scalable=no,width=device-width,height=device-height"}]
               ;[:script {:type "text/coffeescript" :src
               ;"/js/app.coffee"}]
               ]
              [:body
               [:div {:class "container"}
                content]]))
