(ns todoodl.views.index
  (:require [todoodl.views.common :as common]
            [ring.util.response :as ring]
            [noir.response :as response])
  (:use noir.core
        hiccup.core
        hiccup.page-helpers))

(defpartial todo-item []
  [:li {:class "todo-item" "data-foreach-todo" "Todo.all"}
   [:input {:type "checkbox" "data-bind" "todo.done" "data-event-change" "todo.doSave"}]
   [:h4 {:class "task-name" "data-bind" "todo.task" "data-addclass-done" "todo.done"}]
   [:a {:href "#" :class "delete" "data-event-click" "todo.destroy"} "X"]]
  )

(defpartial todo-list []
  [:div
   [:ul {:id "items"}
    (todo-item)
    ]]
  )

(defpartial items-count []
  [:div {:class "one-third column"}
    [:h1 {:class "one columns" "data-bind" "Todo.remainingTasks.length"}
     ]
    [:h3 {:class "one columns" "data-bind" "'task' | pluralize Todo.remainingTasks.length"}]])

(defpartial new-todo-form []
  [:form {"data-formfor-todo" "controllers.todos.emptyTodo" "data-event-submit" "controllers.todos.create"}
   [:input {:class "new-item" :placeholder "Add another task" :data-bind "todo.task"}]]
  )

(defpage "/oldskool" []
  (common/layout
   [:div {:class "sixteen columns"}
    [:h1 {:class "remove-bottom"} "Todoodl"]
    ]
   [:div {:class "one-third column"}
    (new-todo-form)
    (todo-list)
    ]
   (items-count)
   ))

(ring/file-response "index.html" {:root "public"})

(defpage "/todoodl.appcache" []
  (ring/content-type
   (ring/resource-response "todoodl.appcache" {:root "public"})
   "text/cache-manifest"
   )
  )

(def *count* 0)

(defpage "/count" []
  (response/json {:count *count*}))
