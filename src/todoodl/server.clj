(ns todoodl.server
  (:require [noir.server :as server]
            [org.danlarkin.json :as json]
            [clojure.contrib.string :as string]
            ))

; This is a little debugging middleware.  It's really of no use,
; except for debugging Batman, at times.
;(defn backbone [handler]
;
;  (fn [req]
;    (let [headers (str (get-in req [:headers "content-type"]))
;          neue (if (= "application/x-www-form-urlencoded, application/x-www-form-urlencoded" headers)
;                 (let [body (slurp (:body req))]
;                   (do
;                     (update-in req [:params] assoc :batman (slurp (:body req)))))
;
;
;
;                 (do
;                   (println (slurp (:body req)))
;                   (println headers)
;                   (println
;                    (=
;                     "application/json, application/x-www-form-urlencoded"
;                     headers))
;                   req))]
;
;      (handler neue))
;    ))

;(server/add-middleware backbone)
(server/load-views "src/todoodl/views/")

(defn -main [& m]
  (let [mode (keyword (or (first m) :dev))
        port (Integer. (get (System/getenv) "PORT" "8080"))]
    (server/start port {:mode mode
                        :ns 'todoodl})))

