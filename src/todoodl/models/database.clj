(ns todoodl.models.database
  (:require [clojure.contrib.sql :as sql]))

(def *db* {
           :classname "org.sqlite.JDBC"
           :subprotocol "sqlite"
           :subname "db.sqlite3"
           :create true
           })

(def *table-definitions* [])

(defn add-definition [table-def]
  (def *table-definitions*
    (conj *table-definitions* table-def))
  )

(. Class (forName "org.sqlite.JDBC")) ;Initialize the JDBC driver

(defn db-init
  "Creates all the tables that have been added to *table-definitions*"
  []
  (doseq [table-def *table-definitions*]
    (sql/with-connection *db*
      (eval table-def)
      )
    )
  )

(defn query [sql]
  (sql/with-connection *db*
    (sql/with-query-results results [sql]
      (into [] results)))
  )

(defn find-with-query [table query-params]
  (query 
   (str
    "SELECT * FROM " table
    " WHERE " query-params
    )
   ))

(defn all [table]
  (query (str "select * from " table))
  )

(defn find-by-id [table id]
  (first (find-with-query table (str "ID=" id " LIMIT 1")))
  )

