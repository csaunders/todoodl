(ns todoodl.models.todo
  (:require [todoodl.models.database :as db]
            [clojure.contrib.sql :as sql]))

(db/add-definition
 '(sql/create-table
   :todo
   [:id :integer "PRIMARY KEY AUTOINCREMENT"]
   [:task "text"]
   [:done :boolean])
 )

(defn all []
  (db/all "todo"))

(defn find-by-id [id]
  (let [id (if (string? id) (read-string id) id)]
    (db/find-by-id "todo" id)
   )
  )

(defn find-by-name [taskname]
  (first (db/find-with-query "todo" (str "task='" taskname "' LIMIT 1")))
  )

(defn done? [task]
  (= 1 (:done task)))

(defn toggle [task]
  (let [done (:done task)]
    (conj task {:done (if (= 1 done) 0 1)}))
  )

(defn create [taskname]
  (do
   (sql/with-connection db/*db*
     (sql/insert-records
      :todo
      {:task taskname :done false})
     )
   (find-by-name taskname))
  )

(defn update [task]
  (sql/with-connection db/*db*
    (sql/update-or-insert-values
     :todo
     [(str "id=" (:id task))]
     task
     )
    )
  )

(defn delete [task]
  (sql/with-connection db/*db*
    (sql/delete-rows
     :todo
     ["id=?" (:id task)]
     ))
  )
